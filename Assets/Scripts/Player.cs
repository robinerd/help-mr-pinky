﻿using MLAgents;
using MLAgents.Sensors;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class Player : Agent
{
    public bool humanInput = false;
    public float speed = 10;
    public float maxAcceleration = 5;
    public float acceleration = 2;
    public float jumpForce = 100;
    public int hp = 100;
    public Collider2D groundChecker;
    public GameObject deathEffect;

    public UnityEvent OnRespawn = new UnityEvent();

    private Rigidbody2D body;
    private Vector2 spawnPos;
    private int maxHP;
    private int moveDirection;
    private bool jump;
    private ContactFilter2D groundContactFilter;
    private bool grounded = false;
    private bool hasJumped = false;

    bool isTraining { get { return Academy.Instance.IsCommunicatorOn; } }

    // Start is called before the first frame update
    void Start()
    {
        maxHP = hp;
        spawnPos = transform.position;
        body = GetComponent<Rigidbody2D>();
        groundContactFilter = new ContactFilter2D();
        groundContactFilter.SetLayerMask(1 << LayerMask.NameToLayer("Ground"));
        
        StartCoroutine(DecisionLoop());

        Academy.Instance.AutomaticSteppingEnabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        if(jump && grounded)
        {
            jump = false;
            hasJumped = true;
            body.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
        }
    }

    Collider2D[] overlappedColliders = new Collider2D[10];
    private bool checkGrounded()
    {
        int groundsFound = groundChecker.OverlapCollider(groundContactFilter, overlappedColliders);
        return groundsFound > 0;
    }

    private void FixedUpdate()
    {
        grounded = checkGrounded();
        if (moveDirection != 0)
        {
            var targetVelocity = Mathf.Sign(moveDirection) * speed * Vector2.right;
            var missingVelocity = targetVelocity - body.velocity;
            missingVelocity.y = 0;
            if (missingVelocity.magnitude > maxAcceleration)
                missingVelocity = missingVelocity.normalized * maxAcceleration;

            body.AddForce(missingVelocity * acceleration * body.mass);
        }
        else
        {
            body.velocity = new Vector2(body.velocity.x * 0.95f, body.velocity.y);
        }
    }

    public void Win()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void Die()
    {
        if (hp == 0)
            return;

        hp = 0;
        //StopAllCoroutines();
        body.isKinematic = true;
        body.velocity = Vector2.zero;
        GetComponentInChildren<SpriteRenderer>().enabled = false;
        StartCoroutine(HandleDeath());
    }

    IEnumerator HandleDeath()
    {
        deathEffect.SetActive(true);
        yield return new WaitForSecondsRealtime(2);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void Respawn(bool endEpisode)
    {
        transform.position = spawnPos;
        body.velocity = Vector2.zero;
        hp = maxHP;

        if(endEpisode)
            EndEpisode();

        OnRespawn.Invoke();
    }

    private void OnDrawGizmos()
    {
        if (grounded)
        {
            Gizmos.color = Color.cyan;
            Gizmos.DrawSphere(groundChecker.transform.position, 0.3f);
        }
    }

    public IEnumerator DecisionLoop()
    {
        while(gameObject)
        {
            yield return new WaitForSeconds(0.1f);
            RequestDecision();
        }
    }

    public override void OnEpisodeBegin()
    {
        if (humanInput || isTraining)
        {
            base.OnEpisodeBegin();
            Respawn(false);
        }
    }

    public override float[] Heuristic()
    {
        int moveDirection;
        var move = Input.GetAxis("Horizontal");
        if (move > 0.2f)
            moveDirection = 1;
        else if (move < -0.2f)
            moveDirection = -1;
        else
            moveDirection = 0;

        bool jump = false;
        if (Input.GetKey(KeyCode.Space) && grounded)
            jump = true;

        return new float[]{jump ? 1 : 0, moveDirection};
    }
    
    public override void CollectObservations(VectorSensor sensor)
    {
        sensor.AddObservation(hasJumped);
        sensor.AddObservation(moveDirection);
        sensor.AddObservation(body.velocity);
        sensor.AddObservation(grounded);
    }

    public override void OnActionReceived(float[] vectorAction)
    {
        base.OnActionReceived(vectorAction);

        AddReward(0.1f * body.velocity.x);

        float shouldJump = vectorAction[0];
        float movement = vectorAction[1];

        if (shouldJump > 0.5f && grounded)
            jump = true;

        if (movement > 0.5f)
            moveDirection = 1;
        else if (movement < -0.5f)
            moveDirection = -1;
        else moveDirection = 0;

        hasJumped = false;
    }
}
