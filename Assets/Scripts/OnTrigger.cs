﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnTrigger : MonoBehaviour
{
    LayerMask layersEntered = -1;
    public UnityEvent OnEnter = new UnityEvent();

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (layersEntered == (layersEntered | (1 << collision.gameObject.layer)))
        {
            OnEnter.Invoke();
        }
    }
}
