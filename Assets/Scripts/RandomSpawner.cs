﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpawner : MonoBehaviour
{
    public int minCount = 2, maxCount = 4;
    public float minScale = 0.8f, maxScale = 1.3f;
    public GameObject[] prefabs;
    public float radius = 2;

    // Start is called before the first frame update
    void Start()
    {
        Spawn();
        FindObjectOfType<Player>().OnRespawn.AddListener(ClearAndSpawn);
    }

    public void ClearAndSpawn()
    {
        Clear();
        Spawn();
    }
    
    public void Spawn()
    {
        int count = Random.Range(minCount, maxCount + 1);
        for(int i = 0; i < count; i++)
        {
            var prefab = prefabs[Random.Range(0, prefabs.Length)];
            var instance = Instantiate(prefab, 
                                       (Vector2)transform.position + Random.insideUnitCircle * radius * transform.lossyScale.x,
                                       Quaternion.Euler(0,0,Random.Range(0,360)),
                                       transform);

            instance.transform.localScale *= Random.Range(minScale, maxScale);
        }
    }

    public void Clear()
    {
        var childrenToDestroy = new List<Transform>();
        foreach(Transform child in transform)
        {
            childrenToDestroy.Add(child);
        }

        foreach(Transform child in childrenToDestroy)
        {
            Destroy(child.gameObject);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = new Color(0.2f, 0.5f, 1, 0.3f);
        Gizmos.DrawSphere(transform.position, radius * transform.lossyScale.x);
    }
}
