﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndButtons : MonoBehaviour
{
    public void PlayAgain()
    {
        var music = GameObject.Find("Music");
        if (music)
            Destroy(music);

        SceneManager.LoadScene(0);
    }

    public void Exit()
    {
        Application.Quit();
    }
}
