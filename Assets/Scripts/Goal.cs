﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goal : MonoBehaviour
{
    public bool isTrainingGoal = false;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "Player")
        {
            if(isTrainingGoal)
                other.GetComponentInParent<Player>().Respawn(true);
            else
                other.GetComponentInParent<Player>().Win();
        }
    }
}
