﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DraggableBody : MonoBehaviour
{
    public float force = 100;
    public GameObject dragEffect;

    private bool grabbing = false;
    private Rigidbody2D body;
    private float originalDrag;
    private Color originalColor;

    private void Start()
    {
        body = transform.parent.GetComponent<Rigidbody2D>();
        originalDrag = body.drag;
        originalColor = dragEffect.GetComponent<SpriteRenderer>().color;

        UpdateParticles();
    }

    private void OnMouseEnter()
    {
        var col = originalColor;
        col.r += 0.4f;
        col.b += 0.2f;
        col.a = 1;
        dragEffect.GetComponent<SpriteRenderer>().color = col;
    }

    private void OnMouseDown()
    {
        if(!grabbing)
            Grab();
    }

    private void OnMouseExit()
    {
        //if (grabbing)
        //    Release();
        if(!grabbing)
            dragEffect.GetComponent<SpriteRenderer>().color = originalColor;
    }

    private void OnMouseUp()
    {
        if(grabbing)
            Release();
    }

    public void Grab()
    {
        grabbing = true;
        body.drag = 2;
        UpdateParticles();
        dragEffect.GetComponent<ParticleSystem>().Emit(10);
    }

    public void Release()
    {
        body.drag = originalDrag;
        grabbing = false;
        UpdateParticles();
        dragEffect.GetComponent<SpriteRenderer>().color = originalColor;
    }

    private void FixedUpdate()
    {
        if (grabbing)
        {
            Vector2 toMouse = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
            if (toMouse.magnitude > 1)
                toMouse.Normalize();
            Vector2 force = toMouse * this.force;
            body.AddForceAtPosition(force, transform.position);
        }
    }

    private void UpdateParticles()
    {
        var emission = dragEffect.GetComponent<ParticleSystem>().emission;
        emission.rateOverTime = grabbing ? 20 : 4;
        emission.rateOverDistanceMultiplier = grabbing ? 2 : 0;
    }
}
