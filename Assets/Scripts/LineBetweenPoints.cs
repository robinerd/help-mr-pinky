﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineBetweenPoints : MonoBehaviour
{
    public Transform point1, point2;

    // Update is called once per frame
    void FixedUpdate()
    {
        GetComponent<LineRenderer>().SetPositions(new Vector3[] { point1.position, point2.position });
    }
}
