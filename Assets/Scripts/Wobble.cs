﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wobble : MonoBehaviour
{
    public float decayX = 0.92f, decayY = 0.92f;
    public float freqX = 3, freqY = 4;
    [Range(0,0.5f)]
    public float amplitude = 0.1f;
    public float impactSensitivity = 1.0f;

    Rigidbody2D body;
    Vector3 origScale;
    float wobbleX;
    float wobbleY;
    Vector2 prevVelocity;
    float timeX;
    float timeY;

    private void Start()
    {
        body = GetComponentInParent<Rigidbody2D>();
        origScale = transform.localScale;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        var impulse = body.velocity - prevVelocity;
        wobbleX += Mathf.Abs(impulse.x) * impactSensitivity;
        wobbleY += Mathf.Abs(impulse.y) * impactSensitivity;

        wobbleX *= decayX;
        wobbleY *= decayY;

        timeX += (wobbleX + 0.3f * wobbleY) * freqX / Time.fixedDeltaTime;
        timeY += (wobbleY + 0.1f * wobbleY) * freqY / Time.fixedDeltaTime;

        Vector3 scale = origScale;
        float currentAmplitudeX = amplitude * wobbleX;
        float currentAmplitudeY = amplitude * wobbleY;
        scale.x *= Mathf.Lerp(1 / (1 + currentAmplitudeX), 1 + currentAmplitudeX, 0.5f + 0.5f * Mathf.Sin(timeX));
        scale.y *= Mathf.Lerp(1 / (1 + currentAmplitudeY), 1 + currentAmplitudeY, 0.5f + 0.5f * Mathf.Sin(timeY));
        transform.localScale = scale;

        prevVelocity = body.velocity;
    }
}
